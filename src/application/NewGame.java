package application;

import java.net.URL;
import java.util.ResourceBundle;
import engine.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class NewGame implements Initializable {

	@FXML
	private ComboBox<String> cboxTheme;
	@FXML
	private ComboBox<String> cboxDiff;
	@FXML
	private TextField txtColumns;
	@FXML
	private TextField txtRows;
	@FXML
	private Button btnOk;
	@FXML
	private Label lblError;
	@FXML 
	private Label lblError1;
	@FXML
	private Button btnStart;
	@FXML
	private Button splayer;
	@FXML
	private Button mplayer;
	@FXML
	private CheckBox chbox1;
	@FXML
	private CheckBox chbox2;
	@FXML
	private CheckBox chbox3;

	//v�rdierne der skal v�re i comboboxene inds�ttes i arraylists
	ObservableList<String> list1 = FXCollections.observableArrayList("Medieval", "Vulcano", "Jungle", "China", "Italy");
	ObservableList<String> list2 = FXCollections.observableArrayList("Hard", "Easy");

	//arraylist'enes v�rdier inds�ttes i comboboxene og den f�rste v�rdi v�lges
	//klassen husker de tidligere valgte regler og opdaterer checkboxene p� den baggrund
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		cboxTheme.setItems(list1);
		cboxTheme.getSelectionModel().selectFirst();
		cboxDiff.setItems(list2);
		cboxDiff.getSelectionModel().selectFirst();
		
		if (flyingKings == true) {
			chbox1.setSelected(true);
		}
		if (menMoveBack == true) {
			chbox2.setSelected(true);			
		}
		if (menCaptureBack == true) {
			chbox3.setSelected(true);
		}
	}

	public void Return(ActionEvent event) throws Exception {
		Main.LoadScene("application/MainMenu.fxml","application/application.css");
		Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
	}

	public static boolean singleplayer;

	//ved tryk p� singleplayer aktiveres sv�rhedsgrad, og man kan starte spillet
	public void Singleplayer(ActionEvent event) throws Exception {
		Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
		singleplayer = true;

		cboxDiff.setDisable(false);
		btnStart.setDisable(false);

		splayer.setStyle(
				"-fx-text-fill: yellow;" +
				"-fx-font-size: 20px;" );
		mplayer.setStyle(
				"-fx-text-fill: FFDEAD;" +
						"-fx-font-family: 'Arial Black';" +
				"-fx-font-weight: bold ");
	}

	//her deaktiveres sv�rhedsgrad, og man kan starte spillet
	public void Multiplayer(ActionEvent event) throws Exception {
		Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
		singleplayer = false;

		cboxDiff.setDisable(true);
		btnStart.setDisable(false);

		mplayer.setStyle(
				"-fx-text-fill: yellow;" +
				"-fx-font-size: 20px;" );
		splayer.setStyle(
				"-fx-text-fill: FFDEAD;" +
						"-fx-font-family: 'Arial Black';" +
				"-fx-font-weight: bold ");
	}

	public static boolean menMoveBack;
	public static boolean menCaptureBack;
	public static boolean flyingKings;
	
	//ved kryds i afkrydsningsfelt sl�s de tre specielle regler til eller fra
	public void flyingKings(ActionEvent event) throws Exception {
		if (chbox1.isSelected()) {
			flyingKings = true;
		}
		else {
			flyingKings = false;
		}
	}
	
	public void menMoveBack(ActionEvent event) throws Exception {
		if (chbox2.isSelected()) {
			menMoveBack = true;
		}
		else {
			menMoveBack = false;
		}
	}
	
	public void menCaptureBack(ActionEvent event) throws Exception {
		if (chbox3.isSelected()) {
			menCaptureBack = true;
		}
		else {
			menCaptureBack = false;
		}
	}

	public static int theme;
	public static int difficulty;

	//Resten af de valgte v�rdier gemmes, og hvis nogle gyldige v�rdier er valgt, starter spillet, ellers f�r brugeren en fejlbesked
	public void Start(ActionEvent event) throws Exception {

		try {
			MainController.width = Integer.parseInt(txtColumns.getText());
			MainController.height = Integer.parseInt(txtRows.getText());

			if (cboxTheme.getValue() == "Medieval") {
				theme = 0;
			}
			else if (cboxTheme.getValue() == "Jungle") {
				theme = 1;
			}
			else if (cboxTheme.getValue() == "China") {
				theme = 2;
			}
			else if (cboxTheme.getValue() == "Italy") {
				theme = 3;
			}
			else {
				theme = 4;
			}

			if (cboxDiff.getValue() == "Easy") {
				difficulty = 0;
			}
			else {
				difficulty = 1;
			}
	
			if (MainController.width >= 8 && MainController.width % 2 == 0 && MainController.height >= 8 && MainController.width % 2 == 0) {
				Main.startGame();
			}
			else {
				lblError.setVisible(true);
				lblError1.setVisible(true);

			}

		} catch (Exception e) {
			lblError.setVisible(true);
			lblError1.setVisible(true);

		}
		Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
	}
}
