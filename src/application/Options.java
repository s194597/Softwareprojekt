package application;

import java.net.URL;
import java.util.ResourceBundle;

import engine.Main;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.VBox;

public class Options implements Initializable {

	@FXML
	private Slider sliderMusic;
	@FXML
	private Slider sliderSound;
	@FXML
	private CheckBox cbMusic;
	@FXML
	private CheckBox cbSound;
	@FXML
	private VBox optionsbg;


	//Sliderens v�rdi knyttes til mediaPlayerMusic's volumen
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		sliderMusic.setValue(Main.mediaPlayerMusic.getVolume() * 100);
		sliderMusic.valueProperty().addListener(new InvalidationListener() {

			@Override
			public void invalidated(Observable observable) {
				Main.mediaPlayerMusic.setVolume(sliderMusic.getValue() / 100);	
			}
		});	
	}

	//ved tryk p� mute s�ttes sliderens v�rdi til 0 eller 100
	public void Mute(ActionEvent event) throws Exception {
		Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
		if (cbMusic.isSelected()) {
			sliderMusic.setValue(Main.mediaPlayerMusic.getVolume() + 100);
			sliderMusic.setDisable(false);
		}

		else {
			sliderMusic.setValue(Main.mediaPlayerMusic.getVolume() * 0);
			sliderMusic.setDisable(true);
		}
	}

	public void Return(ActionEvent event) throws Exception {
		Main.LoadScene("application/MainMenu.fxml","application/application.css");
		Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
	}
	
	//hvis man �bner options fra et spil, forts�tter man spillet ved at klikke 'back to game'
	public void Continue(ActionEvent event) throws Exception {
		MainController.gameType = 2;
		MainController.simpDam = 0;
		Main.startGame();
		Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
	}
}
