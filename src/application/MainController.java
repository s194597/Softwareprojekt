package application;
import engine.Main;

import java.io.File;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ResourceBundle;

import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.ImageCursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCombination;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.util.Duration;
import logic.GameState;



public class MainController implements Initializable {

	@FXML
	private Button btnContinue;
	public static int gameType;
	public static int width = 8;
	public static int height = 8;

	//N�r hovedmenuen �bner deaktiveres 'Continue', hvis der ikke er et gemt spil
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		if (btnContinue != null) {
			if (GameState.exists("previous")) {
				btnContinue.setDisable(false);
			}
			else if (!GameState.exists("previous")) {
				btnContinue.setDisable(true);
			}
		}

	}

	//ActionEvents i denne klasse �bner alle sammen scener vha. loadScene()-funktionen i main
	//ved tryk p� enhver knap k�rer playSound(), der spiller en klik-lyd
	public static int simpDam;
	public void SimpDam(ActionEvent event) throws Exception {
		//??????????????????
		gameType = 0;
		simpDam = 1;
		Main.startGame();
		Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
	}

	//n�r continue klikkes, s�ttes gameType til 2, hvilket betyder at et gemt spil indl�ses
	public void Continue(ActionEvent event) throws Exception {
		gameType = 2;
		simpDam = 0;
		Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
		Main.startGame();
	}

	//gameType 1 betyder, at et spil starter p� ny
	public void NewGame(ActionEvent event) throws Exception {
		gameType = 1;
		simpDam = 0;
		Main.LoadScene("application/NewGame.fxml", "application/application.css");
		Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
	}

	public void Options(ActionEvent event) throws Exception {
		Main.LoadScene("application/Options.fxml","application/application.css");
		Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");

	}
	public void Optionsingame(ActionEvent event) throws Exception {
		Main.LoadScene("application/Optionsingame.fxml","application/application.css");
		Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");

	}

	public void Credits(ActionEvent event) throws Exception {
		Main.LoadScene("application/Credits.fxml","application/application.css");
		Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
	}

	//i mods�tning til de andre knapper �bner exit en stage, og beholder 'window'-stagen �ben
	public void Exit(ActionEvent event) throws Exception {

		Stage primaryStage = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("Exit.fxml"));
		Scene scene = new Scene(root,500,200);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.getIcons().add(new Image("https://i.imgur.com/atQZf6L.png"));
		Image image = new Image("https://i.imgur.com/1jwlaws.png");
		scene.setCursor(new ImageCursor(image));
		primaryStage.setTitle("Exit");
		primaryStage.setScene(scene);
		primaryStage.show();

		Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");

	}

	//ved tryk p� denne knap lukkes hele applikationen
	public void ExitYes(ActionEvent event) {
		Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
		Platform.exit();
	}

	//her lukkes den nye stage
	public void ExitNo(ActionEvent event) {
		Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
		Stage stage;
		stage = (Stage) ((Button)event.getSource()).getScene().getWindow();
		stage.close();
	}

	//Return bruges til at returnere til hovedmenuen fra indstillinger, credits og new game
	public void Return(ActionEvent event) throws Exception {
		Main.LoadScene("application/MainMenu.fxml","application/application.css");
		Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
	}




}
