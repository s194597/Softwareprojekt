package logic;
import engine.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.ImageCursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.TextAlignment;

import java.util.ArrayList;

import application.MainController;
import application.NewGame;
import logic.CheckPiece;

//SE BUNDEN AF DOKUMENTET!!!!!!!!!!!!!!!!!!!!!!!!!!!
public class Game {

	public static boolean singlePlayer;
	public static AI ai;

	public static Color CheckPieceColor1;
	public static Color CheckPieceColor2;
	public static PieceLog pieceLog = new PieceLog();
	public static Handler handler;

	public static int difficulty = 1;

	public static boolean menMoveBack = true;
	public static boolean menCaptureBack = true;
	public static boolean flyingKings = true;
	public static boolean endGame;

	public static boolean simpDam;

	static int theme;

	public static GameCamera gameCam;
	public static int player1Score = 0;
	public static int player2Score = 0;

	public static int time;

	//checkCanMove()-funktionen unders�ge om der er en spiller, der ikke kan rykke en eneste brik
	public static boolean checkCanMove() {
		for (CheckPiece c : CheckPiece.allPieces) {
			if (c.getTeam() == handler.getTurnTeam()) {
				if (c.getAllMoves().size() > 0) {
					return true;
				}
			}	
		}
		return false;
	}

	//hvis man spiller singleplayer k�res Undo() og Redo() to gange, da computerens, tr�k ogs� tilbagekaldes
	public static void undo() {
		AIcd = 0;
		pieceLog.Undo();;
		if (singlePlayer && handler.getTurnTeam() == 2) { pieceLog.Undo();}
	}
	public static void redo() {
		AIcd = 0;
		pieceLog.Redo();
		if (singlePlayer && handler.getTurnTeam() == 2) { pieceLog.Redo();}
	}

	public static final int TIME_TO_MOVE = 120;
	public static double timeCounter = 0;
	static double timeToMove1 = TIME_TO_MOVE;
	static double timeToMove2 = TIME_TO_MOVE;
	static double player1UsedTime;
	static double player2UsedTime;
	public static int winner;
	static double AIcd;
	static double player1Time;
	static double player2Time;

	//spillet resettes og loades med gemte indstillinger
	public static void start() {
		//Spillet nulstilles
		gameReset();

		gameCam = new GameCamera(true);
		LoadState(MainController.gameType);

		ai = new AI(difficulty);
		createInGameUI();
		handler.thisAction(null);

	}
	static Label team1Points;
	static Label team2Points;
	static Label team1Time;
	static Label team2Time;
	static Label timeLab;

	//Scoreboarded og in-game knapper tilf�jes, samt tilpasset cursor
	public static void createInGameUI() {
		Image image = new Image("https://i.imgur.com/1jwlaws.png");
		Main.front.setCursor(new ImageCursor(image));

		createUpperInGameUI();
		createBottomInGameUI();

	}

	public static void LoadState(int id) {
		switch(id) {
		case 0:
			//Nyt spil: simpdam
			simpDam = true;
			GameState.Spawn(GameState.simpDam(Main.n, Main.n));
			break;

		case 1:
			//Nyt spil: advanced dam
			GameState.Spawn(GameState.newGame(MainController.width,MainController.height));
			break;

		case 2:
			//forts�t spil fra sidst gang
			GameState.Spawn(GameState.loadState("previous"));
			break;
		}
	}
	//Forsinkelse tilf�jes til tiden p� scoreboarded
	public static int roundTime(double time) {
		return (int)(time +0.5);
	}

	static double zoomSpeed = 8;
	static double rotateSpeed = 2;
	public static void update() {

		//N�r der ikke er flere brikker tilbage popper slutspils boearded op
		if (player1Score == MainController.width/2*3) {
			winner = 1;
			Main.setPause(true);
			createEndGameUI();

		}
		if (player2Score == MainController.width/2*3) {
			winner = 2;
			Main.setPause(true);
			createEndGameUI();
		}

		//Der tjekkes hvis tur det er, hvis en af spillerne ikke kan bev�ge sig vinder den modsatte 
		if (handler.turn % 2 == 0 && checkCanMove() == false) {
			winner = 2;
			createEndGameUI();
		}
		if (handler.turn % 2 != 0 && checkCanMove() == false) {
			winner = 1;
			createEndGameUI();
		}


		timeCounter += Main.getDeltaTime();
		//kameraets styrefunktioner. Piletasterne bruges her
		if (Main.keyInput.getKey(KeyCode.UP)) {
			gameCam.rotateVertical(-rotateSpeed);
		}
		if (Main.keyInput.getKey(KeyCode.DOWN)) {
			gameCam.rotateVertical(rotateSpeed);
		}
		if (Main.keyInput.getKey(KeyCode.LEFT)) {
			gameCam.rotateHorizontal(-rotateSpeed);
		}
		if (Main.keyInput.getKey(KeyCode.RIGHT)) {
			gameCam.rotateHorizontal(rotateSpeed);
		}
		//I og 0 for zoom funktioner
		if (Main.keyInput.getKey(KeyCode.I)) {
			gameCam.zoom(zoomSpeed);
		}
		if (Main.keyInput.getKey(KeyCode.O)) {
			gameCam.zoom(-zoomSpeed);
		}
		//spillet g�r hovedmenuen hvis der trykkes p� ESC
		if (Main.keyInput.getKeyDown(KeyCode.ESCAPE)) {
			GameState.saveState("previous", GameState.getCurrentState());
			Main.LoadScene("application/MainMenu.fxml","application/application.css");
		}
		//J og K for undo og redo
		if (Main.keyInput.getKeyDown(KeyCode.J)) {
			undo();
		}
		if (Main.keyInput.getKeyDown(KeyCode.K)) {
			redo();
		}

		//Tager overordnet tid samt tid for hver spillers tr�k
		if (handler.turn % 2 == 0) {
			timeToMove2=TIME_TO_MOVE;
			timeToMove1 -= Main.getDeltaTime();
			player1UsedTime += Main.getDeltaTime();

			timeLab.setText("Player 1's turn! \nGametime: " + roundTime(timeCounter));
			team1Time.setText("Player 1 time to make move:\n  " + roundTime(timeToMove1));
			team2Time.setText("Player 2 time to make move:\n  " + roundTime(timeToMove2));

			//hvis spiller ikke n�r at bev�ge sig, laves et tilf�ldigt tr�k
			if (timeToMove1 <= 0) {
				handler.makeRandomMove();
			}

		}

		else {
			//Holder styr p� CPU'en
			AIcd += Main.getDeltaTime();
			if (singlePlayer && AIcd >= 1.5) {
				AIcd = 0;
				ai.makeMove();
			}
			timeToMove1 = TIME_TO_MOVE;
			timeToMove2 -= Main.getDeltaTime();
			player2UsedTime += Main.getDeltaTime();

			timeLab.setText("Player 2's turn! \nGametime: " + roundTime(timeCounter));
			team2Time.setText("Player 2 time to make move:\n  " + roundTime(timeToMove2));
			team1Time.setText("Player 1 time to make move:\n  " + roundTime(timeToMove1));
			if (timeToMove2 <= 0) {
				handler.makeRandomMove();
			}
		}
		team1Points.setText("Player 1 score: \n   " + player1Score);
		team2Points.setText("Player 2 score: \n   " + player2Score);



	}
	//Hver gang der dr�bes en brik tilf�jes et point til holdets scoreboard
	public static void onKill(CheckPiece c) {
		if (c.getTeam() == 2) {
			player1Score++;
		}
		else {
			player2Score++;
		}
	}


	//themeSelect v�lger hvilket tema der skal i brug ud fra hvilken int den modtager som input
	public static void themeSelect(int theme) {
		Rectangle bg = new Rectangle(Main.windowX,Main.windowY);
		Image Medieval = new Image ("https://i.imgur.com/EkP42b5.jpg");
		Image China = new Image ("https://i.imgur.com/E0aqahh.jpg");
		Image Rome = new Image ("https://i.imgur.com/NRmoC9H.jpg");
		Image Vulcano = new Image ("https://i.imgur.com/iXefvO8.gif");
		Image Forrest = new Image ("https://i.imgur.com/EsibECL.gif");
		Main.behind.getStylesheets().add("logic/inGameUI.css");
		Main.behind.getChildren().add(bg);
		switch(theme) {
		case 0:
			//Medieval Theme
			CheckPieceColor1 = Color.GOLD;
			CheckPieceColor2 = Color.SILVER;
			ImagePattern medieval = new ImagePattern(Medieval);
			bg.setFill(medieval);
			break;
		case 1:
			//Jungle Theme
			CheckPieceColor1 = Color.FLORALWHITE;
			CheckPieceColor2 = Color.ROSYBROWN;
			ImagePattern forrest = new ImagePattern(Forrest);
			bg.setFill(forrest);
			break;
		case 2:
			//Chinese Theme
			CheckPieceColor1 = Color.GOLD;
			CheckPieceColor2 = Color.WHITESMOKE;
			ImagePattern chinese = new ImagePattern(China);
			bg.setFill(chinese);

			break;
		case 3:
			//Italian Theme
			CheckPieceColor1 = Color.RED;
			CheckPieceColor2 = Color.GOLD;
			ImagePattern rome = new ImagePattern(Rome);
			bg.setFill(rome);
			break;
		case 4:
			//Vulcano theme
			CheckPieceColor1 = Color.DIMGRAY;
			CheckPieceColor2 = Color.ORANGE;
			ImagePattern vulcano = new ImagePattern(Vulcano);
			bg.setFill(vulcano);
			break;
		}
	}
	public static Grid mainGrid;

	//Implementeringen af in-game knapperne 
	public static void createUpperInGameUI(){
		int spacing = 30;
		// Implementering af Undo knap
		Button undo = new Button();
		undo.setId("undo");
		undo.setPrefSize(48, 48);
		undo.setLayoutX(Main.windowX-undo.getPrefWidth()-8*spacing);
		undo.setLayoutY(0);
		undo.getStylesheets().add("logic/inGameUI.css");

		//Redo knap
		Button redo = new Button();
		redo.setId("redo");
		redo.setPrefSize(48, 48);
		redo.setLayoutX(Main.windowX-redo.getPrefWidth()-6*spacing);
		redo.getStylesheets().add("logic/inGameUI.css");

		//Eventhandler som styrer knappen n�r der trykkes p� den.
		undo.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
				undo();

			}
		});

		redo.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
				redo();

			}
		});

		//Mute knap
		ToggleButton mute = new ToggleButton();
		mute.setId("mute");
		mute.setPrefSize(48, 48);
		mute.setLayoutX(Main.windowX-mute.getPrefWidth()-4*spacing);
		mute.setFocusTraversable(false);
		mute.getStylesheets().add("logic/inGameUI.css");

		mute.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				if (mute.isSelected()) {
					Main.mediaPlayerMusic.pause();
				}

				else {
					Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
					Main.mediaPlayerMusic.play();

				}

			}
		});
		//Pause knap
		ToggleButton pause = new ToggleButton();
		pause.setId("pause");
		pause.setPrefSize(48, 48);

		pause.setLayoutX(Main.windowX-mute.getPrefWidth()-12*spacing);
		pause.setFocusTraversable(false);
		pause.getStylesheets().add("logic/inGameUI.css");

		//Vi inds�tter en usynlig hbox p� sk�rmen s� n�r der trykkes p� pause knappen kan intet trykkes p� pga hboxen.
		HBox pausescreen = new HBox();
		Label pauselbl = new Label("PAUSED");
		pausescreen.setFocusTraversable(false);
		pauselbl.setFocusTraversable(false);
		//pausescreen.setPrefSize(200, 200);
		pauselbl.setMinSize(300,300);
		pausescreen.setMinSize(Main.windowX, Main.windowY*0.87);
		pauselbl.setId("pauselbl");
		pauselbl.getStylesheets().add("logic/inGameUI.css");
		pausescreen.setId("pausescreen");
		pausescreen.getStylesheets().add("logic/inGameUI.css");
		pauselbl.setAlignment(Pos.TOP_CENTER);
		pausescreen.setAlignment(Pos.CENTER);
		pausescreen.setLayoutX(0);
		pausescreen.setLayoutY(Main.windowY/2*0.3);
		pauselbl.setLayoutY(300);

		pause.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				if (pause.isSelected()) {
					pausescreen.setFocusTraversable(false);
					pauselbl.setFocusTraversable(false);
					Main.mediaPlayerMusic.pause();
					pausescreen.getChildren().add(pauselbl);
					Main.front.getChildren().add(pausescreen);

					Main.setPause(true);
				}

				else {
					Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
					pausescreen.setFocusTraversable(false);
					pauselbl.setFocusTraversable(false);
					Main.mediaPlayerMusic.play();
					pausescreen.getChildren().remove(pauselbl);
					Main.front.getChildren().remove(pausescreen);
					Main.setPause(false);
				}

			}
		});

		//Options knap
		Button options = new Button();
		options.setId("options");
		options.setPrefSize(48, 48);
		options.setLayoutX(Main.windowX-redo.getPrefWidth()-2*spacing);
		options.getStylesheets().add("logic/inGameUI.css");

		options.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
				GameState.saveState("previous", GameState.getCurrentState());
				Main.LoadScene("application/Optionsingame.fxml","application/application.css");
				Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");


			}

		});

		//Home knap
		Button home = new Button();
		home.setId("home");
		home.setPrefSize(48, 48);
		home.setLayoutX(Main.windowX-redo.getPrefWidth());
		home.getStylesheets().add("logic/inGameUI.css");

		home.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
				GameState.saveState("previous", GameState.getCurrentState());
				Main.LoadScene("application/MainMenu.fxml","application/application.css");
			}
		});

		//Smallscreen knap
		ToggleButton small = new ToggleButton();
		small.setId("small");
		small.setPrefSize(48, 48);
		small.setLayoutX(Main.windowX-mute.getPrefWidth()-10*spacing);
		small.setFocusTraversable(false);
		small.getStylesheets().add("logic/inGameUI.css");

		small.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				if (small.isSelected()) {
					Main.window.setFullScreen(false);
					Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
				}

				else {
					Main.window.setFullScreen(true);
					Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
				}
			}
		});
		Main.front.getChildren().addAll(pause, undo, redo, mute, options, home, small);
	}

	//Implementering af scoreboarded in-game
	private static void createBottomInGameUI() {
		// TODO Auto-generated method stub

		//Labels
		timeLab = new Label("Time");
		team1Points = new Label("Player 1 score: \n        " + player1Score);
		team2Points = new Label("Player 2 score: \n       "  + player2Score);
		team1Time = new Label("Player 1 time to move: \n  "  + TIME_TO_MOVE);
		team2Time = new Label("Player 2 time to move\n  "  + TIME_TO_MOVE);

		//Stylesheet
		team1Points.getStylesheets().add("logic/inGameUI.css");
		team2Points.getStylesheets().add("logic/inGameUI.css");
		timeLab.getStylesheets().add("logic/inGameUI.css");

		//id
		timeLab.setId("time");
		team1Points.setId("team1");
		team2Points.setId("team2");
		team1Time.setId("team1");
		team2Time.setId("team2");

		//alignment
		timeLab.setAlignment(Pos.TOP_CENTER);
		team1Points.setAlignment(Pos.TOP_CENTER);
		team2Points.setAlignment(Pos.TOP_CENTER);
		team1Time.setAlignment(Pos.TOP_CENTER);
		team2Time.setAlignment(Pos.TOP_CENTER);

		//size
		timeLab.autosize();
		team1Points.autosize();
		team2Points.autosize();
		timeLab.setPrefSize(Main.windowX/3, Main.windowY);
		timeLab.setMinWidth(200);
		team1Points.setPrefSize(Main.windowX/3, Main.windowY);
		team2Points.setPrefSize(Main.windowX/3, Main.windowY);
		team1Time.setPrefSize(Main.windowX/3, Main.windowY);
		team2Time.setPrefSize(Main.windowX/3, Main.windowY);
		team1Time.setMinWidth(280);
		team2Time.setMinWidth(280);

		//HBox
		HBox lowerhbox = new HBox();
		lowerhbox.setPrefSize(Main.windowX,Main.windowY/12);
		lowerhbox.setId("lower");
		lowerhbox.getStylesheets().add("logic/inGameUI.css");
		lowerhbox.setAlignment(Pos.CENTER);
		lowerhbox.setSpacing(40);;
		lowerhbox.getChildren().addAll(team1Points,team1Time,timeLab,team2Time,team2Points);
		lowerhbox.setLayoutX(0);
		lowerhbox.setLayoutY(Main.windowY-lowerhbox.getPrefHeight());

		Main.front.getChildren().addAll(lowerhbox);
	}

	//Implementering af slut spils sk�rmens som skal poppe op n�r spillet er slut.
	public static void createEndGameUI() {
		if (!endGame) {
			endGame = true;
			//boxes
			VBox endGameUI = new VBox();
			HBox title = new HBox();
			HBox undertitle = new HBox();
			HBox hboxLabel = new HBox();
			HBox spacing = new HBox();
			HBox hboxButtons = new HBox();

			//labels&buttons
			Label endText = new Label("Player "+winner+" wins! \n");
			Label statistics = new Label("Statistics");
			Label score = new Label("Score \nPlayer 1: "+ player1Score + " points\nPlayer 2: " + player2Score + " points\nPoints overall: "+(player1Score+player2Score) +" \n \n ");
			Label time = new Label("Time \nPlayer 1 spent: " + roundTime(player1UsedTime)+ " sec\nPlayer 2 spent: "+ roundTime(player2UsedTime) + " sec \nTotal time spent: "+ roundTime(timeCounter) +" sec \n \n");
			Button playAgain = new Button("Play Again");
			Button MainMenu = new Button("Back to MainMenu");

			//space & size
			statistics.setPrefSize(500, 10);
			hboxLabel.setSpacing(0);
			hboxLabel.setPrefSize(100, 100);
			spacing.setPrefSize(40, 20);;
			undertitle.setPrefHeight(50);
			hboxButtons.setSpacing(100);
			hboxButtons.setMinSize(100, 100);
			score.setPrefWidth(150);
			time.setPrefWidth(150);
			score.setMinSize(280, 100);
			time.setMinSize(280,100);

			//id&alignment
			endText.setId("endText");
			endText.setTextAlignment(TextAlignment.CENTER);
			endText.getStylesheets().add("logic/inGameUI.css");
			statistics.setId("statistics");
			statistics.setTextAlignment(TextAlignment.CENTER);
			statistics.getStylesheets().add("logic/inGameUI.css");
			score.setId("scoretime");
			score.setTextAlignment(TextAlignment.CENTER);
			score.getStylesheets().add("logic/inGameUI.css");
			time.setId("scoretime");
			time.setTextAlignment(TextAlignment.CENTER);
			time.getStylesheets().add("logic/inGameUI.css");
			playAgain.setId("playAgain");
			playAgain.getStylesheets().add("logic/inGameUI.css");
			MainMenu.setId("mainMenu");
			MainMenu.getStylesheets().add("logic/inGameUI.css");
			hboxLabel.setFillHeight(true);;

			//center
			score.setAlignment(Pos.CENTER);
			time.setAlignment(Pos.CENTER);
			title.setAlignment(Pos.TOP_CENTER);
			statistics.setAlignment(Pos.CENTER);
			undertitle.setAlignment(Pos.TOP_CENTER);
			hboxLabel.setAlignment(Pos.TOP_CENTER);
			hboxButtons.setAlignment(Pos.CENTER);

			//buttons
			playAgain.setId("hboxbtns");
			playAgain.setPrefSize(185, 50);
			MainMenu.setId("hboxbtns");
			MainMenu.setPrefSize(185, 50);

			//setonAction

			//Play Again
			playAgain.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent e) {
					Main.front.getChildren().remove(endGameUI);
					MainController.gameType = 1;
					Main.startGame();
					Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
				}

			});
			//Go back to Main menu
			MainMenu.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent e) {
					GameState.delete("previous");
					Main.LoadScene("application/MainMenu.fxml","application/application.css");
					Main.playSound("sound/Button-Click-Off-SoundBible.com-1730098776.mp3");
				}
			});

			//vbox
			endGameUI.setAlignment(Pos.CENTER);
			endGameUI.setPrefSize(525, 350);
			endGameUI.setMinSize(500, 400);
			//endGameUI.setPadding(new Insets(0,0,0,0));
			endGameUI.setId("endGame");
			endGameUI.setLayoutX(Main.windowX/2 - endGameUI.getPrefWidth()*0.5);
			endGameUI.setLayoutY(Main.windowY/2 - endGameUI.getPrefHeight()*0.7);
			endGameUI.getStylesheets().add("logic/inGameUI.css");

			//adding
			title.getChildren().add(endText);
			undertitle.getChildren().add(statistics);
			hboxLabel.getChildren().addAll(score, time);
			endGameUI.getChildren().addAll(title, spacing, undertitle, hboxLabel,hboxButtons);
			if (!simpDam) {
				hboxButtons.getChildren().add(playAgain);
			}
			hboxButtons.getChildren().add(MainMenu);
			Main.front.getChildren().add(endGameUI);
		}
	}

	//alle variabler resettes, s� spillet kan starte forfra
	public static void gameReset() {
		GameObject.DestroyAll();
		Main.setPause(false);
		handler = new Handler();
		player1Score = 0;
		player2Score = 0;
		time = 0;
		CheckPiece.allPieces = new ArrayList<CheckPiece>();
		endGame = false;
		simpDam = false;
	}

}