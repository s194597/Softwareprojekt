package logic;

import java.util.ArrayList;
import java.util.Random;

public class Handler {
	
	/**
	 * Finder de nye brikker der kan rykkes og kalder 'setGlow' for dem der kan.
	 */
	private void action(CheckPiece lastMoved, int skip) {
		for (CheckPiece c : CheckPiece.allPieces) {
			c.setGlow(false);
		}
		//hvis man lige har hoppet s� se om man kan hoppe igen
		if (lastMoved != null) {
			if (lastMoved.getJumps().size() > 0) {
				selectable = new ArrayList<CheckPiece>();
				selectable.add(lastMoved);
			}
			else {
				mustJump = false;
			}	
		}
		//hvis der ikke kan hoppes er det den andens tur
		if (!mustJump()) {
			turn +=skip;
			findSelectable();
		}
		if (!Game.singlePlayer || getTurnTeam() == 1) {
			for (CheckPiece c : selectable) {
				c.setGlow(true);
			}
		}
	}
	
	public void nextAction(CheckPiece lastMoved) {
		action(lastMoved,1);
	}
	public void thisAction(CheckPiece lastMoved) {
		action(lastMoved,0);
	}

	//turen
	public int turn;

	boolean mustJump;
	public boolean mustJump() {
		return mustJump;
	}

	ArrayList<CheckPiece> selectable;
	public ArrayList<CheckPiece> getSelectable(){
		return selectable;
	}
	
	/**
	 * finder brikkerne der har tr�k og tilf�jer dem til 'selectable'
	 */
	public void findSelectable() {
		int team = getTurnTeam();
		
		ArrayList<CheckPiece> canJump = new ArrayList<CheckPiece>();
		ArrayList<CheckPiece> canMove = new ArrayList<CheckPiece>();
		for (CheckPiece c : CheckPiece.allPieces) {
			if (c.getTeam() == team) {
				if (c.getJumps().size() > 0) {
					canJump.add(c);	
				}else if (c.getShortMoves().size() > 0) {
					canMove.add(c);
				}
			}
		}
		if (canJump.size() > 0) {
			mustJump = true;
			selectable = canJump;
		}else {
			mustJump = false;
			selectable = canMove;
		}
	}
	/**
	 * Returner det hold, hvis tur det er
	 */
	public int getTurnTeam() {
		if (turn % 2 == 0) {
			return 1;
		}
		return 2;
	}
	/**
	 * fortager et tilf�ldigt tr�k for den person, hvis tur det er.
	 */
	public void makeRandomMove() {
		Random r = new Random();
		CheckPiece p = selectable.get(r.nextInt(selectable.size()));
		p.makeMoveTo(p.getAllMoves().get(r.nextInt(p.getAllMoves().size())));
	}
}
