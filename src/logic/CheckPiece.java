package logic;

import java.util.ArrayList;

import engine.GameObject;
import engine.Main;
import engine.Renderer;
import engine.Utility;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Cylinder;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;

public class CheckPiece extends GameObject {

	//H�jden som brikken falder ned fra n�r brikken dannes
	static int startOffsetZ = 140;

	//Info om brikkens cyllinder
	Cylinder model;
	static int radius = 11;
	static int height = 8;

	//Alle checkPieces i spillet
	static ArrayList<CheckPiece> allPieces = new ArrayList<CheckPiece>();

	public CheckPiece(String name, int team, Grid grid, int _gridX, int _gridY) {
		super(name, null, new Translate(0,0,startOffsetZ));
		PieceData pd = new PieceData(name,team,_gridX,_gridY,false); //Add to contructor: king
		createPiece(grid,pd);

	}
	public CheckPiece(Grid grid, PieceData pd) {
		super(pd.name, null, new Translate(0,0,startOffsetZ));
		createPiece(grid, pd);
	}

	private void createPiece(Grid grid, PieceData pd) {
		data = pd;
		this.grid = grid;
		this.occupyTile(this.getTile());
		allPieces.add(this);

		model = new Cylinder(radius, height);
		setRenderer(new Renderer(model));
		setTransforms(position,new Rotate(90, Rotate.X_AXIS));
		setToDefaultColor();

		makeKing(getData().king);

		//S� brikken kan trykkes p�:
		addEventFilter(MouseEvent.MOUSE_CLICKED, Controller.mouseClick);
		addEventFilter(MouseEvent.MOUSE_ENTERED, Controller.mouseHoverEntered);
		addEventFilter(MouseEvent.MOUSE_EXITED, Controller.mouseHoverExited);
	}

	private Grid grid;

	//Information om brikken inholdes i PieceData 'data':
	private PieceData data;
	public final int getTeam() {
		return data.team;
	};
	public final int getGridX() {
		return data.gridX;
	}
	public int getGridY() {
		return data.gridY;
	}
	public boolean isKing() {
		return data.king;
	}
	public final PieceData getData() {
		return data;
	}

	/**
	 * Brikkens position p� grid �ndres.
	 */
	public void setGridPos(int gx, int gy) {
		unOccupyTile();
		data.gridX = gx;
		data.gridY = gy;
		occupyTile();
	}


	double movementSpeed = 3.5;
	public void moveTowardsAnimation(double tx, double ty) {
		setPosition(
				Utility.moveTowards(position.getX(), tx, movementSpeed),
				Utility.moveTowards(position.getY(), ty, movementSpeed),
				Utility.moveTowards(position.getZ(), height/2.0 + Grid.THICKNESS/2.0, movementSpeed)
				);

	}

	public void setToDefaultColor() {
		if (getTeam() == 1) { setColor(Game.CheckPieceColor1);}
		else { setColor(Game.CheckPieceColor2);}
	}
	public Color getDefaultColor() {
		if (getTeam() == 1) { return Game.CheckPieceColor1;}
		return Game.CheckPieceColor2;
	}
	/**
	 * Brikken optager feltet som den er p�.
	 */
	public void occupyTile() {
		getTile().checkPiece = this;
	}
	/**
	 * Brikken optager ikke feltet som den er p�.
	 */
	public void unOccupyTile() {
		getTile().checkPiece = null;
	}
	public void occupyTile(Tile t) {
		t.checkPiece = this;
	}
	public void unOccupyTile(Tile t) {
		t.checkPiece = null;
	}


	public void setGlow(boolean state) {
		if (state) { setColor(Color.DARKSEAGREEN);}
		else {setToDefaultColor();}
	}

	public Tile getTile() {
		return grid.getTile(getGridX(), getGridY());
	}

	//Om denne briks mulige tr�k:
	public ArrayList<Tile> getAllMoves() {
		return getTile().getAllLegalMoves(getData());
	}
	public ArrayList<Tile> getShortMoves(){
		return getTile().getLegalShortMoves(getData());
	}

	public ArrayList<Tile> getJumps(){
		return getTile().getLegalJumps(getData());
	}

	//felterne i 'tiles' bliver highlightet.
	public void ShowMoves(ArrayList<Tile> tiles) {
		for(Tile t : tiles) {
			t.setColor(getDefaultColor());
		}
	}
	/**
	 * Brikken fortager et tr�k.
	 */
	public void makeMoveTo(Tile t) {
		ArrayList<CheckPiece> deadPieces = new ArrayList<CheckPiece>();
		if  (getTile().getLegalJumps(getData()).contains(t)) {
			ArrayList<Tile> row = getTile().tilesTo(t);

			//logic for dr�bte brikker:
			for (Tile dt : row) {
				if (dt.isOccupied() && dt.checkPiece.getTeam() != getTeam()) {
					deadPieces.add(dt.checkPiece);
				}
			}
		}else if (!getTile().getLegalShortMoves(getData()).contains(t)) {
			//Hvis tr�kket ikke er lovligt sker der intet
			return;
		}
		//logik for dr�bte brikker
		if (deadPieces.size() > 0) {
			ArrayList<PieceData> data = new ArrayList<PieceData>();
			for (CheckPiece c : deadPieces) {
				data.add(new PieceData(c));
			}
			Game.pieceLog.addFrom(new PieceData(this), data);
			for (CheckPiece c : deadPieces) {
				c.killThis();
			}
		}else {
			Game.pieceLog.addFrom(new PieceData(this), null);
		}
		Main.playSound("sound/click.mp3");
		removeHighlight();
		setGridPos(t.gridX, t.gridY);
		if (getTile().kingTile(getTeam())) {
			makeKing(true);
		}
		Game.pieceLog.addTo(new PieceData(this));
		Game.handler.nextAction(this);
	}

	public void makeKing(boolean b) {
		if (b) {
			if (!isKing()) {
				Main.playSound("sound/heaven.mp3");
			}
			model.setHeight(height*5);
		}
		else {
			model.setHeight(height);
		}
		data.king = b;
	}

	/**
	 * Dr�ber denne brik
	 */
	public void killThis() {
		unOccupyTile();
		allPieces.remove(this);
		Main.playSound("sound/sword.mp3");
		Game.onKill(this);
		Destroy(this);
	}


	public void removeHighlight() {
		ArrayList<Tile> legalTiles = getAllMoves();
		for (Tile t : legalTiles) {
			t.setToDefaultColor();
		}
	}

	public void update() {
		//Animation s�ledes at brikke langsom bev�ger sig hen til der hvor den skal v�re 
		moveTowardsAnimation(grid.getTilePosX(getGridX()),grid.getTilePosY(getGridY()));
	}

}
