package logic;

import engine.GameObject;
import application.NewGame;
import engine.Main;
import engine.Renderer;
import engine.Utility;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Material;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.Cylinder;
import javafx.scene.transform.Translate;

public class Grid {

	//Kan man trykket sig ud af braettet?
	//Braet stoerrelse
	public static final int THICKNESS = 20;
	public static final int TILE_SIZE = 32;
	public int width = 8;
	public int height = 8;
	public static Color PLAYED_TILE;
	public static Color NOT_PLAYED_TILE;

	Tile[][] tiles;

	//Board UI
	//Tile
	public Tile getTile(int x, int y) {
		if (!inGrid(x,y)) {
			return null;
		}
		return tiles[x][y];
	}

	public Grid(int h, int w) {
		height = h;
		width = w;
		CreateGrid();
	} 
	public void CreateGrid() {


		themeSelect(NewGame.theme);

		tiles = new Tile[width][height];
		for (int x = 0; x < width; x++) {
			for(int y = 0; y < height; y++) {

				Tile tile = new Tile(this,x,y);
				tiles[x][y] = tile;
				tile.setToDefaultColor();
			}
		}
	}
	public void removeHighlight() {
		for (Tile[] t : tiles) {
			for (Tile tt : t) {
				tt.setToDefaultColor();
			}
		}
	}

	public boolean getTileOccupied(int gx, int gy) {

		return inGrid(gx,gy) && Game.mainGrid.getTile(gx, gy).checkPiece ==null;
	}

	public boolean inGrid(int gx, int gy) {
		return gx < width && gx >= 0
				&& gy < height && gy >= 0;
	}

	public double getTilePosX(int x) {
		return -(TILE_SIZE * x - TILE_SIZE*width/2.0+TILE_SIZE/2.0);
		//return this.x - worldSize/2f + worldSize*x/(float)size;
	}

	public double getTilePosY(int y) {
		return -(TILE_SIZE * y - TILE_SIZE*height/2.0+TILE_SIZE/2.0);
		//return this.y - worldSize/2f + worldSize*y/(float)size;
	}

	public void themeSelect(int theme) {
		switch(theme) {
		case 0:
			//Classic Theme
			PLAYED_TILE = Color.BLACK;
			NOT_PLAYED_TILE = Color.WHITE;
			break;
		case 1:
			//Jungle Theme
			PLAYED_TILE = Color.SADDLEBROWN;
			NOT_PLAYED_TILE = Color.DARKGREEN;
			break;
		case 2:
			//Chinese Theme 
			PLAYED_TILE = Color.DEEPSKYBLUE;
			NOT_PLAYED_TILE = Color.RED;
			break;
		case 3:
			//Italian Theme 
			PLAYED_TILE = Color.LIMEGREEN;
			NOT_PLAYED_TILE = Color.ANTIQUEWHITE;
			break;
		case 4:
			//Vulcano Theme 
			PLAYED_TILE = Color.FIREBRICK;
			NOT_PLAYED_TILE = Color.ORANGERED;
			break;
		}
	}
}
