package logic;

import engine.GameObject;
import engine.Main;
import engine.Renderer;
import engine.Utility;
import javafx.scene.Camera;
import javafx.scene.Parent;
import javafx.scene.PerspectiveCamera;
import javafx.scene.PointLight;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape3D;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;

public class GameCamera extends GameObject{

	public GameCamera(boolean setCamera) {
		super("",null, new Translate(0,0,0));
		camera = new PerspectiveCamera(true);
		if (setCamera) {
			set();
		}
	}
	public Camera get() {
		return camera;
	}
	/**
	 * Camera s�ttes p� spillescenen
	 */
	public void set() {
		Main.gameScene.setCamera(camera);
	}

	Camera camera;

	Rotate angleX = new Rotate(0,Rotate.X_AXIS);
	Rotate angleZ = new Rotate(0,Rotate.Z_AXIS);
	
	PointLight light;
	
	public void start() {
		//Kameraets instilles:
		camera.setNearClip(1);
		camera.setFarClip(10000);
		
		light = new PointLight(Color.WHITE);
		getChildren().add(light);

		getChildren().add(camera);
		setTransforms(angleZ, angleX,position);

		zoom(0);
		rotateHorizontal(0);
		rotateVertical(0);
	}
	//De forskellige vinkler
	double zAngle = 180;
	double xAngle = 230;

	//m�ngden af zoom
	double zoom = -600;

	public void rotateHorizontal(double dz) {
		zAngle += dz;
		angleZ.setAngle(zAngle);
	}
	public void rotateVertical(double dx) {
		xAngle += dx;
		xAngle = Utility.Clamp(xAngle, 180, 250);
		angleX.setAngle(xAngle);
	}
	public void zoom(double amount) {
		zoom += amount;
		zoom = Utility.Clamp(zoom, -2500, -120);
		setPosition(0, 0, zoom);
	}

}
