package logic;

import logic.CheckPiece;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import logic.Tile;

public class Controller {

	public static CheckPiece selected;

	public static EventHandler<MouseEvent> mouseClick  = new EventHandler<MouseEvent>(){
		@Override
		public void handle(MouseEvent event) {

			unSelect();

			CheckPiece clicked = (CheckPiece)event.getSource();

			if (Game.handler.getSelectable().contains(clicked)) {
				//Man kan ikke trykket p� og styre komputerens brikker
				if (!Game.singlePlayer || Game.handler.getTurnTeam() == 1) {
					select(clicked);
				}
			}
		}
	};

	public static EventHandler<MouseEvent> mouseHoverEntered  = new EventHandler<MouseEvent>(){

		@Override
		public void handle(MouseEvent event) {

			//Farve n�r man holder musen over en brik p� ens hold
			CheckPiece g = (CheckPiece)event.getSource();
			if (!Game.singlePlayer) {
				if (Game.handler.getTurnTeam() == g.getTeam()) {
					g.setColor(Color.SPRINGGREEN);
				}
			}else {
				if (Game.handler.getTurnTeam() == 1 && g.getTeam() == 1) {
					g.setColor(Color.SPRINGGREEN);
				}
			}
		}

	};

	public static EventHandler<MouseEvent> mouseHoverExited  = new EventHandler<MouseEvent>(){

		@Override
		public void handle(MouseEvent event) {
			//Farven fra hover skal fjernes
			CheckPiece g = (CheckPiece)event.getSource();
			if (g != selected) {
				g.setToDefaultColor();
			}
		}
	};

	public static EventHandler<MouseEvent> moveToTile = new EventHandler<MouseEvent>(){

		@Override
		public void handle(MouseEvent event) {

			Tile t = (Tile)event.getSource();

			if (selected != null && t.getColor().equals(selected.getDefaultColor())) {
				//Hvis en brik er selected skal den bev�ge til det felt der trykkes p�
				selected.removeHighlight();
				selected.makeMoveTo(t);
			}

		} 
	};


	public static void select(CheckPiece c) {
		if (c != null) {
			selected = c;
			selected.setColor(Color.SPRINGGREEN);
			selected.ShowMoves(selected.getAllMoves());
		}
	}

	public static void unSelect() {
		if (selected != null) {
			selected.removeHighlight();
			selected.setToDefaultColor();
			selected = null;
		}
	}

	boolean mustJump = false;

	public void makeRandomMove() {

	}
}
