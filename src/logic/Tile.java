package logic;

import java.util.ArrayList;

import engine.GameObject;
import engine.Renderer;
import engine.Utility;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Box;
import javafx.scene.transform.Translate;

public class Tile extends GameObject{

	public Grid grid;
	public int gridX, gridY;
	public CheckPiece checkPiece = null;

	public Tile(Grid grid, int gridX, int gridY) {
		super(
				"tile",
				new Renderer(new Box(Grid.TILE_SIZE,Grid.TILE_SIZE,Grid.THICKNESS)),
				new Translate(grid.getTilePosX(gridX), grid.getTilePosY(gridY),0)
				);

		this.grid = grid;
		this.gridX = gridX;
		this.gridY = gridY;

		addEventFilter(MouseEvent.MOUSE_CLICKED, Controller.moveToTile);
	}


	public Color getDefaultColor() {
		if((gridX + 1 -(gridY%2))%2 == 0) {return Grid.PLAYED_TILE;}
		else {return Grid.NOT_PLAYED_TILE;}
	}

	public void setToDefaultColor() {
		setColor(getDefaultColor());
	}
	/**
	 * Unders�ger om dette er et kong felt for et hold
	 */
	public boolean kingTile(int team) {
		if (team == 1 && gridY == grid.height - 1 || team == 2 && gridY == 0) {
			return true;
		}else {return false;}
	} 
	/**
	 * Returnerer nabofeltet.
	 */
	public Tile neighbor(int dx, int dy) {
		return grid.getTile(gridX + dx, gridY + dy);
	}

	public boolean isNeighbor(int dx, int dy) {
		return neighbor(dx, dy) != null;
	}
	public boolean isNeighbor(Tile t) {
		return neighbor(t.gridX - gridX, t.gridY - gridY) != null;
	}
	/**
	 * Unders�ger om der er en brik p� feltet.
	 */
	public boolean isOccupied() {
		return checkPiece != null;
	}
	/**
	 * returnerer naboerne til feltet (kun naboer der st�r skr�t)
	 */
	public ArrayList<Tile> getDiagNeighbors(){
		ArrayList<Tile> ret = new ArrayList<Tile>();
		Tile n = neighbor(1, 1);
		if (n != null) {ret.add(n);}

		n = neighbor(-1, 1);
		if (n != null) {ret.add(n);}

		n = neighbor(-1, -1);
		if (n != null) {ret.add(n);}

		n = neighbor(1, -1);
		if (n != null) {ret.add(n);}
		return ret;
	}
	/**
	 * Alle LOVLIGE tr�k
	 */
	public ArrayList<Tile> getAllLegalMoves(PieceData pd) {
		ArrayList<Tile> ret = getLegalShortMoves(pd);
		ret.addAll(getLegalJumps(pd));
		return ret;
	}
	/**
	 * Alle tr�k som ikke er hop.
	 */
	public ArrayList<Tile> getShortMoves(PieceData pd){
		if (Game.handler.mustJump && !Game.simpDam) {
			return new ArrayList<Tile>();
		}
		ArrayList<Tile> ret = new ArrayList<Tile>();
		if (!pd.king) {
			ret.addAll(getFrontNeighbors(pd.team,1));
			if (Game.menMoveBack) {
				ret.addAll(getFrontNeighbors(pd.team,-1));
			}
		}else {
			if (Game.flyingKings){
				ret.addAll(getAllFrontNeighborsInSight(pd.team, 1));
				ret.addAll(getAllFrontNeighborsInSight(pd.team, -1));
			}else{
				ret.addAll(getFrontNeighbors(pd.team,1));
				ret.addAll(getFrontNeighbors(pd.team,-1));
			}
		}

		return ret;
	}

	public ArrayList<Tile> getAllFrontNeighborsInSight(int team, int distance){
		ArrayList<Tile> ret = new ArrayList<Tile>();
		for (Tile t : getAllFrontNeighbors(team, distance)) {
			ret.add(t);
			if (t.isOccupied() && t.checkPiece.getTeam() == team) {
				return ret;
			}
		}
		return ret;
	}

	/**
	 * Alle LOVLIGE tr�k, som ikke er hop
	 */
	public ArrayList<Tile> getLegalShortMoves(PieceData pd){
		return legalMoves(getShortMoves(pd));
	}

	/**
	 * Unders�ger om tr�kket er lovligt
	 */
	public ArrayList<Tile> legalMoves(ArrayList<Tile> moves){
		ArrayList<Tile> ret = new ArrayList<Tile>();
		for (Tile t : moves) {
			if (!t.isOccupied()) {
				ret.add(t);
			}
		}
		return ret;
	}
	/**
	 * Nabofelterne forand brikken
	 */
	public ArrayList<Tile> getFrontNeighbors(int team, int distance){
		if (team == 2) {distance *=-1;}
		ArrayList<Tile> ret = new ArrayList<Tile>();
		if (isNeighbor(distance, distance)) { ret.add(neighbor(distance, distance));}
		if (isNeighbor(-distance, distance)) { ret.add(neighbor(-distance, distance));}
		return ret;
	}
	/**
	 * Nabofelterne forand brikken, men ikke kun et skridt, men uendelige
	 */
	//funktionalitet for flyvende konger
	public ArrayList<Tile> getAllFrontNeighbors(int team, int dir){
		dir = dir/Math.abs(dir);
		ArrayList<Tile> ret = new ArrayList<Tile>();
		for (int i = 1; i <= grid.height; i++) {
			ret.addAll(getFrontNeighbors(team, i*dir));
		}
		return ret;
	}
	//sorted closest to furthest.
	public ArrayList<Tile> getNeighborRow(int dist, int xdir, int ydir){
		xdir = xdir/Math.abs(xdir);
		ydir = ydir/Math.abs(ydir);
		dist = Math.abs(dist);
		ArrayList<Tile> ret = new ArrayList<Tile>();

		for (int i = 1; i <= dist; i++) {
			if (isNeighbor(xdir*i, ydir*i)) {
				ret.add(neighbor(xdir*i, ydir*i));
			}
		}
		return ret;
	}

	/**
	 * Returnere alle hop
	 */
	public ArrayList<Tile> getJumps(PieceData pd){
		ArrayList<Tile> ret = new ArrayList<Tile>();
		int distance = 1;
		int a = 1;
		if (pd.team == 2) {a=-1;}
		if (pd.king && Game.flyingKings) {distance = grid.height;}
		Tile jump = getJump(pd.team, getNeighborRow(distance,a,a));
		if (jump != null) {ret.add(jump);}

		jump = getJump(pd.team, getNeighborRow(distance,-a,a));
		if (jump != null) {ret.add(jump);}
		if (pd.king || Game.menCaptureBack) { // king || all dir for men
			jump = getJump(pd.team, getNeighborRow(distance,a,-a));
			if (jump != null) {ret.add(jump);}

			jump = getJump(pd.team, getNeighborRow(distance,-a,-a));
			if (jump != null) {ret.add(jump);}
		}
		return ret;
	}
	/**
	 * Alle LOVLIGE hop
	 */
	public ArrayList<Tile> getLegalJumps(PieceData pd){
		return legalMoves(getJumps(pd));

	}
	//funktionalitet for flyvende konger
	private Tile getJump(int team, ArrayList<Tile> row) {
		for (Tile t : row) {
			if (t.isOccupied() && t.checkPiece.getTeam() == team) {
				return null;
			}
			if (t.isOccupied() && t.checkPiece.getTeam() != team) {
				Tile jumpTo = t.neighbor(Utility.sign(t.gridX-gridX), Utility.sign(t.gridY-gridY));
				if (jumpTo != null) {
					if (jumpTo.isOccupied() && jumpTo.checkPiece.getTeam() != team) {
						continue;
					}
					return jumpTo;
				}
			}
		}
		return null;
	}
	/**
	 * Alle felter mellem to felter
	 */
	public ArrayList<Tile> tilesTo(Tile t){
		ArrayList<Tile> row = getNeighborRow(t.gridX - gridX, t.gridX - gridX, t.gridY-gridY);
		row.remove(t);
		return row;
	}


}
