package logic;

import java.util.ArrayList;

public class AI {
	int difficulty;
	public AI(int difficulty) {
		this.difficulty = difficulty;
	}

	public void makeMove() {
		//Hvis det er easy er tr�kkene tilf�dligt, ellers baseres tr�kkende p� en algoritme
		if (difficulty == 0) {
			Game.handler.makeRandomMove();
		}else {
			makeCalculatedMove();
		}
	}

	public void makeCalculatedMove() {
		CheckPiece bestPiece = null;
		Tile bestTile = null;
		int bestScore = -100000;

		for (CheckPiece c : Game.handler.selectable) {
			Tile[] tile = new Tile[] {Game.mainGrid.getTile(c.getGridX(), c.getGridY())};
			int score = getBestScore(new PieceData(c), tile, false);

			if (score > bestScore) {
				bestScore = score;
				bestTile = tile[0];
				bestPiece = c;
			}
		}
		System.out.println("AI - score of best move: " + bestScore);
		if (bestPiece != null && bestTile != null) {
			bestPiece.makeMoveTo(bestTile);
		}
	}
	private int getBestScore(PieceData pd, Tile[] ret, boolean jump) {
		Tile tile = ret[0];
		ArrayList<Tile> jumps = tile.getLegalJumps(pd);	
		if (jumps.size() == 0) {
			if (!jump) {
				int bestScore = -1000000;
				for (Tile t : tile.getLegalShortMoves(pd)) {
					//tilv�ksten i score
					int score = getPositionalScore(t, pd) - getPositionalScore(tile, pd);
					if (score > bestScore) {
						ret[0] = t;
						bestScore = score;
					}
				}
				return bestScore;
			}else {	
				return getPositionalScore(ret[0], pd);
			}
		}
		int bestScore = -100000;
		for (Tile t : jumps) {
			int score = 500;
			if (score > bestScore) {
				ret[0] = t;
				bestScore = score;
			}
		}
		return bestScore;
	}

	private int getPositionalScore(Tile tile, PieceData pd) {
		int score = 0;
		ArrayList<Tile> neighbors = tile.getDiagNeighbors();
		if (!pd.king) {
			//Det er godt at komme t�ttere p� konge feltet
			score -= 10*tile.gridY;
		}
		Tile original = Game.mainGrid.getTile(pd.gridX, pd.gridY);
		for (Tile t : neighbors) {
			if (t == original) {
				continue;
			}
			if (t.isOccupied()) {
				if (t.checkPiece.getTeam() == pd.team) {
					//Det er godt at v�re ved siden af brikker fra samme hold
					score += 10;

					Tile neiTwo = t.neighbor((t.gridX - tile.gridX), (t.gridY - tile.gridY));
					if (neiTwo != null && neiTwo.isOccupied() && neiTwo.checkPiece.getTeam() != pd.team) {
						//det er meget godt at beskytte en brik fra at blive hoppet over
						score += 300;
					}

				}else {
					Tile neiTwo = tile.neighbor(-(t.gridX - tile.gridX), -(t.gridY - tile.gridY));
					if (neiTwo != null && neiTwo != original && neiTwo.isOccupied() && neiTwo.checkPiece.getTeam() == pd.team) {
						//Det er fint at v�re ved siden af en fjentlig brik med backup
						score += 170;
					}
					//Det er d�rligt at v�re ved siden af en fjentlig brik uden backup (for s� kan de formentlig dr�be din brik)
					score -= 200;
				}
			}
		}
		return score;
	}

}
