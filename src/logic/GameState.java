package logic;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.LinkOption;

import application.NewGame;
import engine.GameObject;
import engine.Utility;

public class GameState implements Serializable{
	//Spil variabler og indstillinger:
	int width, height;
	int turn;
	double gameTime;
	double p1Time, p2Time;
	boolean singlePlayer;
	
	int difficulty;
	boolean flyingKings;
	boolean menMoveBack;
	boolean menCaptureBack;
	
	int theme;
	
	PieceData[] pieces;	
	//undo-redo historikken gemmes ogs�
	PieceLog pieceLog = new PieceLog();
	public GameState() {}
	public GameState(int h, int w) {height = h; width = w;}
	
	/**
	 * Opretter en GameState som beskriver nuv�rende spil
	 */
	public static GameState getCurrentState() {
		GameState gs = new GameState(Game.mainGrid.height,Game.mainGrid.width);
		gs.turn = Game.handler.turn;
		gs.pieceLog = Game.pieceLog;
		gs.gameTime = Game.timeCounter;
		gs.p1Time = Game.player1UsedTime; gs.p2Time = Game.player2UsedTime;
		gs.singlePlayer = Game.singlePlayer;
		gs.theme = Game.theme;
		
		gs.flyingKings = Game.flyingKings; gs.menMoveBack = Game.menMoveBack; gs.menCaptureBack = Game.menCaptureBack;
		gs.difficulty = Game.difficulty;
		
		GameObject[] allg = GameObject.findAll(CheckPiece.class);

		CheckPiece[] allCheckPieces = new CheckPiece[allg.length];
		GameObject.arrayCast(allg, allCheckPieces);

		gs.pieces = new PieceData[allCheckPieces.length];
		for (int i = 0; i < gs.pieces.length;i++) {
			gs.pieces[i] = new PieceData(allCheckPieces[i]);
		}
		return gs;
	}
	//Hvor filen gemmes
	public final static String folderPath = "./game_states/";//"resources/game_states/";
	public final static String extension = ".cgs";
	
	/**
	 * Finder en gemt GameState og loader det ind i hukkomelse
	 */
	public static GameState loadState(String name) {
		try {
			//System.out.println("LOADED!");
			return Utility.loadData(folderPath + name + extension);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
	/**
	 * Gemmer en GameState p� drevet
	 */
	public static void saveState(String name, GameState data) {
		try {
			File directory = new File(folderPath);
			if (!directory.exists()) {
				directory.mkdir();
			}
			//System.out.println(directory.exists());
			Utility.saveData(folderPath+name+extension, data);
			//System.out.println("SAVED!");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	/**
	 * Unders�ger om filen eksisterer
	 */
	public static boolean exists(String name) {
		File f = new File(folderPath+name+extension);
		return f.exists();
	}
	/**
	 * Sletter en fil hvis den eksistere
	 */
	public static void delete(String name) {
		File f = new File(folderPath+name+extension);
		if (f.exists()) {
			f.delete();
		}
	}
	/**
	 * Returnerer en GameState som svarer til et nyt spil med given br�tst�rrelse.
	 */
	public static GameState newGame(int height, int width) {
		boolean king = false;
		GameState ret = new GameState(height, width);
		ret.singlePlayer = NewGame.singleplayer;
		
		ret.flyingKings = NewGame.flyingKings; ret.menMoveBack = NewGame.menMoveBack; ret.menCaptureBack = NewGame.menCaptureBack;
		ret.difficulty = NewGame.difficulty;
		
		ret.theme = NewGame.theme;
		
		ret.pieces = new PieceData[3*width/2 * 2];
		//Player 1
		for (int i = 0; i < width/2; i++) {ret.pieces[i] = new PieceData("p1_"+i,1,i*2+1,0,king);}
		for (int i = 0; i < (width+1)/2; i++) {ret.pieces[i+width/2] = new PieceData("p1_"+i+width/2,1,i*2,1,king);}
		for (int i = 0; i < width/2; i++) {ret.pieces[i+width] = new PieceData("p1_"+i+width,1,i*2+1,2,king);}
		//Player 2
		for (int i = 0; i < width/2; i++) {ret.pieces[i+3*width/2] = new PieceData("p2_"+i+3*width/2,2,i*2,height-1,king);}
		for (int i = 0; i < (width+1)/2; i++) {ret.pieces[i+width/2+3*width/2] = new PieceData("p2_"+i+width/2+3*width/2,2,i*2+1,height-2,king);}
		for (int i = 0; i < width/2; i++) {ret.pieces[i+width+3*width/2] = new PieceData("p2_"+i+width+3*width/2,2,i*2,height-3,king);}
		return ret;
	}
	/**
	 * Returnerer en GameState som svarer til simpDam
	 */
	public static GameState simpDam(int height, int width) {
		GameState ret = new GameState(height, width);
		ret.singlePlayer = false;
		ret.flyingKings = false;
		ret.menCaptureBack = true;
		ret.menMoveBack = true;
				
				
		ret.pieces = new PieceData[2];
		ret.pieces[0] = new PieceData("p1_1",1,0,0,false);
		ret.pieces[1] = new PieceData("p2_1",2,width-1,height-1,false);
		return ret;
	}
	//En af den til vi arbejdede p�, men ikke n�ede at blive f�rdig med.
	public static GameState random(int height, int width) {
		return null;
	}
	/**
	 * Spillet bliver til som beskrevet i GameStaten 'gs'
	 */
	public static void Spawn(GameState gs) {
		Game.handler.turn = gs.turn;
		Game.pieceLog = gs.pieceLog;
		Game.timeCounter = gs.gameTime;
		Game.player1UsedTime = gs.p1Time; Game.player2UsedTime = gs.p2Time;
		Game.singlePlayer = gs.singlePlayer;
		
		Game.flyingKings = gs.flyingKings; Game.menMoveBack = gs.menMoveBack; Game.menCaptureBack = gs.menCaptureBack;
		Game.difficulty = gs.difficulty;
		
		Game.theme = gs.theme;
		Game.themeSelect(Game.theme);
		Game.mainGrid = new Grid(gs.height,gs.width);
	
		for(PieceData pd : gs.pieces) {
			CheckPiece cp = new CheckPiece(Game.mainGrid, pd); //to be tested
			cp.setToDefaultColor();
		}
		Game.undo(); Game.redo();
	}
}
//Data om en brik
class PieceData implements Serializable{
	public PieceData() {}
	public PieceData(String n, int t, int gx, int gy, boolean k) {
		name = n; team = t; gridX = gx; gridY = gy; king = k;
	}
	public PieceData(CheckPiece cp) {
		name = cp.name; team = cp.getTeam(); gridX = cp.getGridX(); gridY = cp.getGridY(); king = cp.isKing();
	}
	public String name;
	public boolean king;
	public int gridX, gridY;
	public int team;	
}
