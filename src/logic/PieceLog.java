package logic;

import java.io.Serializable;
import java.util.ArrayList;
import engine.GameObject;
import javafx.util.Pair;

public class PieceLog implements Serializable {

	public int size() {
		return fromHistory.size();
	}
	//Arraylists der holder styr p� data f�r og efter tr�kket.
	ArrayList<PieceData> fromHistory = new ArrayList<PieceData>();
	ArrayList<PieceData> toHistory = new ArrayList<PieceData>();
	ArrayList<ArrayList<PieceData>> deadPieces = new ArrayList<ArrayList<PieceData>>();
	ArrayList<Pair<Integer,Integer>> fromScore = new ArrayList<Pair<Integer,Integer>>();
	ArrayList<Pair<Integer,Integer>> toScore = new ArrayList<Pair<Integer,Integer>>();
	ArrayList<Integer> turn = new ArrayList<Integer>();

	/**
	 * Tilf�jer data om brikken og spillet f�r tr�kket
	 */
	public void addFrom(PieceData pd, ArrayList<PieceData> piecesDead) {
		for (int i = size()-1; i > location ; i--) {
			fromHistory.remove(i);
			toHistory.remove(i);
			deadPieces.remove(i);
			turn.remove(i);
			fromScore.remove(i);
			toScore.remove(i);
		}
		fromHistory.add(pd);
		deadPieces.add(piecesDead);
		turn.add(Game.handler.turn);
		fromScore.add(new Pair<Integer,Integer>(Game.player1Score, Game.player2Score));
		location = size() - 1;
	}
	/**
	 * Tilf�jer data om brikken og spillet efter tr�kket.
	 */
	public void addTo(PieceData pd) {
		toHistory.add(pd);
		toScore.add(new Pair<Integer,Integer>(Game.player1Score, Game.player2Score));
	}

	//Hvor man befinder sig i undo-redo-historikken.
	int location;


	public void Undo() {
		skip(-1);
	}
	public void Redo() {
		skip(1);
	}
	public void skip(int amount) {

		if (size() > 0) {
			if (amount < 0 && location + amount >= -1) {
				updatePiece(true);
				location += amount;
			}
			else if (amount > 0 && location + amount < size()) {
				location += amount;
				updatePiece(false);
			}
		}
	}
	/**
	 * Updaterer brikken til det nye sted den skal v�re i forhold til redo-undo-historikken
	 */
	private void updatePiece(boolean from) {
		ArrayList<PieceData> history;
		ArrayList<Pair<Integer,Integer>> score;
		if (from) {history = fromHistory;}
		else {history = toHistory;}
		if (from) {score = fromScore;}
		else {score = toScore;}

		PieceData pd = history.get(location);
		ArrayList<PieceData> dps = deadPieces.get(location);

		if (dps != null) {
			if (!from) {
				for (PieceData dp : dps) {
					//en d�d brik skal fjernes n�r man trykker redo
					((CheckPiece)GameObject.find(dp.name)).killThis();
				}
			}else {
				for (PieceData dp : dps) {
					//en d�d brik skal gentilf�jes n�r man trykker undo.
					new CheckPiece(dp.name, dp.team, Game.mainGrid, dp.gridX,dp.gridY);
				}
			}
		}
		CheckPiece cp = (CheckPiece)GameObject.find(pd.name);
		cp.setGridPos(pd.gridX, pd.gridY);
		cp.makeKing(pd.king);

		Game.handler.turn = turn.get(location);
		Game.mainGrid.removeHighlight();

		Game.player1Score = score.get(location).getKey();
		Game.player2Score = score.get(location).getValue();

		if (!from) {
			Game.handler.nextAction(cp);			
		}else {
			Game.handler.thisAction(cp);
		}
	}
}
