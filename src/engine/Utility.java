package engine;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public final class Utility {

	//Denne klasse indholder generelle funktioner der kan bruges af alle klasser

	/**
	 * Gemmer en klasse
	 */
	public static <T> T loadData(String fileName) throws ClassNotFoundException, IOException{
		FileInputStream fileStream = new FileInputStream(fileName);
		ObjectInputStream objectStream = new ObjectInputStream(fileStream);

		T data = (T)objectStream.readObject();

		objectStream.close();
		fileStream.close();

		return data;
	}
	/**
	 * Loader en klasse
	 */
	public static <T>void saveData(String fileName, T data) throws IOException {
		FileOutputStream fileStream = new FileOutputStream(fileName);
		ObjectOutputStream objectStream = new ObjectOutputStream(fileStream);

		objectStream.writeObject(data);

		objectStream.close();
		fileStream.close();
	}
	/**
	 * returnerer tallet 'from' flyttet 'amount' t�tterer p� 'to'
	 */
	public static double moveTowards(double from, double to, double amount) {		
		double d = to - from;
		if (Math.abs(d) < amount) {
			return to;
		}
		return from + Math.signum(d)*amount;
	}
	/**
	 * Holder v�rdien 'value' inden for from og to.
	 */
	public static double Clamp(double value, double from, double to) {
		if (from > to) { throw new IllegalArgumentException();}
		if (value > to) { return to;}
		if (value < from) { return from;}
		return value;

	}
	public static int sign(int i) {
		if (i > 0) {return 1;}
		if (i < 0) {return -1;}
		return 0;
	}


}
