package engine;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Sprite {
	
	//Ideen med at lave en renderer klasse var at man kunne skift mellem 2D og 3D.
	//Dette var noget som vi stadig arbjedede p�.
	
	//Denne klasse indeholder information om et billede
	
	Image image;
	ImageView imageView;

	public Sprite(String imageName, double scaleX, double scaleY) {
		set(imageName, scaleX, scaleY);
	}

	public Sprite(String imageName) {
		set(imageName);
	}

	public ImageView get() {
		return imageView;
	}
	public void set(String imageName, double height, double width) {
		image = new Image(Main.RESOURCE_PATH + imageName);
		imageView = new ImageView(image);
		imageView.setFitHeight(height); 
		imageView.setFitWidth(width); 
	}
	public void set(String imageName) {
		image = new Image(Main.RESOURCE_PATH + imageName);
		imageView = new ImageView(image);
		imageView.setFitHeight(image.getHeight()); 
		imageView.setFitWidth(image.getWidth()); 
	}
}
