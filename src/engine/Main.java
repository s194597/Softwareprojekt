package engine;
import logic.*;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.ImageCursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.PointLight;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCombination;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class Main extends Application{
	public static int windowX = 1280, windowY = 720;

	public static final String RESOURCE_PATH = "Checkers/resources/";

	public final static FileGetter fileGetter = new FileGetter();

	public static KeyInput keyInput;

	public static Stage window;

	public static int n=8;

	public static void main(String[] args) {
		if (args.length == 1) {
			try {
				n = Integer.parseInt(args[0]);
			}catch(Exception e) {}
		}
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {

		window = primaryStage;
		window.setTitle("Checkers");
		window.setResizable(false);
		playMusic("sound/medieval.mp3");
		LoadScene("application/MainMenu.fxml","application/application.css");
	}

	public static void LoadScene(String path_fxml, String path_css) {
		try {
			//Parent root = FXMLLoader.load(Paths.get(path_fxml).toUri().toURL());
			Parent root = FXMLLoader.load(fileGetter.getFXMLURL(path_fxml));
			windowX = 1280; windowY = 720;
			Scene scene = new Scene(root,windowX,windowY,true);

			//System.out.print(fileGetter.getFXMLURL("/application/MainMenu.fxml").toString());
			scene.getStylesheets().add(fileGetter.getFXMLURL(path_css).toExternalForm());
			//Button action on enter key
			scene.getAccelerators().put(
					KeyCombination.valueOf("Enter"), () -> {
						Node focusOwner = scene.getFocusOwner();
						if (focusOwner instanceof Button) {
							((Button) focusOwner).fire();
						}
					});
			window.getIcons().add(new Image("https://i.imgur.com/atQZf6L.png"));
			Image image = new Image("https://i.imgur.com/1jwlaws.png");
			scene.setCursor(new ImageCursor(image));

			window.setScene(scene);
			window.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	public static Scene Scene;
	static Group root;
	public static Group front;
	public static Group behind;
	public static SubScene gameScene;
	public static Group gameRoot;
	static AnimationTimer gameLoop;

	//Pause:
	static boolean pause;
	public static void setPause(boolean state) {
		pause = state;
	}
	public static final boolean getPause() {
		return pause;
	}
	public static void togglePause() {
		setPause(!getPause());
	}

	public static void startGame() {
		//S�rger for der ikke k�re flere gameLoops
		if (gameLoop !=null) {
			gameLoop.stop();
		}

		windowX = 1080;
		windowY = 720;

		windowX = (int) Screen.getPrimary().getBounds().getWidth();
		windowY = (int) Screen.getPrimary().getBounds().getHeight();


		gameRoot = new Group();
		gameScene = new SubScene(gameRoot,windowX,windowY,true, SceneAntialiasing.BALANCED);
		gameScene.setFill(Color.TRANSPARENT);
		root = new Group();
		front = new Group();
		behind = new Group();
		root.getChildren().add(behind);
		root.getChildren().add(gameScene);
		root.getChildren().add(front);
		Scene = new Scene(root,windowX,windowY);

		keyInput = new KeyInput(Scene);

		Game.start();
		startTime = System.nanoTime();
		prevTime = startTime;
		Image image = new Image("https://i.imgur.com/1jwlaws.png"); 
		gameScene.setCursor(new ImageCursor(image));

		gameLoop = new AnimationTimer()
		{
			public void handle(long currTime)
			{
				deltaTime = (currTime-prevTime)/1000000000f;
				prevTime = currTime;
				if (!pause) {
					//Update for GameObjekts kaldes hver frame
					Game.update();

					for(GameObject o : GameObject.getGameObjectRegistry()) {
						o.baseUpdate();
					}
					keyInput.listen();
				}
			}
		};
		gameLoop.start();

		window.setScene(Scene);
		window.setFullScreen(true);
		window.show();
	}

	static long startTime;
	static long prevTime;
	static float deltaTime;

	/**
	 * Tiden der er g�et siden applicationen startede.
	 */
	public static float elapsedTime() {
		return (System.nanoTime()-startTime)/1000000000f;
	}

	/**
	 * Tiden der er g�et fra den sidste frame.
	 */
	public static float getDeltaTime() {
		return deltaTime;
	}

	//Music player
	public static MediaPlayer mediaPlayerMusic;

	public static void playMusic(String path) {
		//Media media = new Media(Paths.get(path).toUri().toString());
		Media media = null;
		try {
			media = new Media(fileGetter.getResourceURL(path).toURI().toString());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mediaPlayerMusic = new MediaPlayer(media);
		mediaPlayerMusic.setAutoPlay(true);
	}

	//Sound effects player
	public static MediaPlayer mediaPlayerSound;

	public static void playSound(String path) {
		//Media media = new Media(Paths.get(path).toUri().toString());
		Media media = null;
		try {
			media = new Media(fileGetter.getResourceURL(path).toURI().toString());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mediaPlayerSound = new MediaPlayer(media);
		mediaPlayerSound.setAutoPlay(true);
	}	

}
