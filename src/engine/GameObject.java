package engine;

import java.util.ArrayList;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.transform.Transform;
import javafx.scene.transform.Translate;


public class GameObject extends Group {

	//public Group group = new Group();

	public String name;
	Renderer renderer;
	public Translate position;

	public GameObject (String name, Renderer renderer, Translate position) {
		this.name = name;
		this.position = position;

		setRenderer(renderer);

		getTransforms().add(position);

		addGameObject(this);
	}

	public Renderer getRenderer() {
		return renderer;
	}

	public void setRenderer(Renderer r) {
		if (renderer != null) {
			getChildren().remove(renderer.get());
		}
		renderer = r;
		if (r != null && !r.nullRenderer()) {
			getChildren().add(renderer.get());
		}

	}
	public void setTransforms(Transform... transforms) {
		getTransforms().clear();
		for (Transform t : transforms) {
			getTransforms().add(t);
		}
	}
	public void setColor(Color c) {
		if (renderer.isShape()) {
			PhongMaterial mat = new PhongMaterial();
			mat.setSpecularColor(c);
			mat.setDiffuseColor(c);
			renderer.getShape().setMaterial(mat);
			color = c;
		}
	}
	Color color;
	public Color getColor() {
		return color;
	}

	public void baseStart(){
		start();
	}
	boolean hasStarted = false;
	public void baseUpdate() {
		if (hasStarted == false) {
			baseStart();
			hasStarted = true;
		}
		update();
	}
	/**
	 * Denne funktion kaldes s� snart GameObjektet skabes.
	 */
	public void start() {}

	/**
	 * Denne funktion kaldes hver frame.
	 */
	public void update() {}

	private boolean isDestroyed = false;

	/**
	 * Sletter GameObjektet
	 */
	public static void Destroy(GameObject g) {
		removeGameObject(g);
		g.isDestroyed = true;
	}
	/**
	 * Flytter dette GameObject med de angivene v�rdier
	 */
	public void translate(double dx, double dy, double dz) {
		setPosition(position.getX() + dx, position.getY() + dy, position.getZ() + dz);
	}
	public void setPosition(double x, double y, double z) {
		position.setX(x); position.setY(y); position.setZ(z);
	}
	
	
	/**
	 * Printer GameObjectets position
	 */
	public void printPosition() {
		System.out.println("(" + position.getX()+", " + position.getY() + ", " + position.getZ() + ")");
	}
	
	/**
	 * Vores array, der holder styr p� hvilke GameObjects der er aktive i scenen
	 */
	private static ArrayList<GameObject> gameObjects = new ArrayList<GameObject>();
	public static ArrayList<GameObject> getGameObjectRegistry(){
		return gameObjects;
	}

	/**
	 * Fjerner et GameObject fra vores array af aktive GameObjects
	 */
	private static void removeGameObject(GameObject g) {
		Group root = (Group)(Main.gameScene.getRoot());
		root.getChildren().remove(g);
		gameObjects.remove(g);
	}
	/**
	 * Tilf�jer et GameObject til vores array af aktive GameObjects
	 */
	private static void addGameObject(GameObject g) {
		Group root = (Group)(Main.gameScene.getRoot());
		root.getChildren().add(g);

		gameObjects.add(g);
	}
	
	/**
	 * Finder alle GameObjekter af en bestemt klasse, som arver fra GameObject
	 */
	public static GameObject[] findAll(Class<?> type) {
		int size = 0;
		for(GameObject o : gameObjects) {
			if (o.getClass() == type) {
				size++;
			}
		}
		GameObject[] ret = new GameObject[size];
		int i = 0;
		for(GameObject o : gameObjects) {
			if (o.getClass() == type) {
				ret[i] = o;
				i++;
			}
		}
		return ret;
	}
	
	/**
	 * Casters et array til et array af en anden type
	 */
	public static <T> void arrayCast(Object[] from, T[] to){
		if (from.length != to.length) {
			throw new IllegalArgumentException("Both arrays must have same length.");
		}
		for (int i = 0; i < to.length; i++) {
			to[i] = (T)from[i];
		}
	}
	
	/**
	 * Fjerner GameObjectet
	 */
	public static void DestroyAll() {
		gameObjects.clear();
		
	}
	
	/**
	 * Finder et GameObjekt med et bestemt navn
	 */
	public static GameObject find(String _name) {
		for (GameObject g : gameObjects) {
			if (g.name.equals(_name)) {
				return g;
			}
		}
		return null;
	}

}