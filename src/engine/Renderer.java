package engine;

import javafx.scene.Node;
import javafx.scene.shape.Shape3D;

public class Renderer {
	
	
	//Ideen med at lave en renderer klasse var at man kunne skift mellem 2D og 3D.
	//Dette var noget som vi stadig arbjedede p�.
	
	private Shape3D shape;
	private Sprite sprite;

	public Renderer(Shape3D shape) {
		set(shape);
	}

	public Renderer(Sprite sprite) {
		set(sprite);
	}

	public void set(Shape3D shape) {
		this.shape = shape;
		this.sprite = null;
	}
	public void set(Sprite sprite) {
		this.sprite = sprite;
		this.shape = null;
	}

	public Shape3D getShape() {
		return shape;
	}
	public Sprite getSprite() {
		return sprite;
	}
	public Node get() {
		if (isShape()) {
			return shape;
		}else {
			return sprite.imageView;
		}
	}

	public boolean isShape() {
		if (shape != null) {
			return true;
		}
		return false;
	}
	public boolean isSprite() {
		if (sprite != null) {
			return true;
		}
		return false;
	}
	public boolean nullRenderer() {
		if (shape != null) {
			return false;
		}
		if (sprite != null) {
			return false;
		}
		return true;
	}
}
