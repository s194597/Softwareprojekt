package engine;

import java.net.URL;

public class FileGetter {
	
	//Bruges til at hente filer
	
	public final String resourcePath = "/";
	public final String fxmlPath = "/";
	
	public URL getResourceURL(String name) {
		return getClass().getResource(resourcePath + name);
	}
	public URL getFXMLURL(String name) {
		return getClass().getResource(fxmlPath + name);
	}
	
}
