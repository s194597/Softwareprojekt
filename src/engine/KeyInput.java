package engine;

import java.util.ArrayList;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class KeyInput {

	ArrayList<KeyCode> down = new ArrayList<KeyCode>();
	ArrayList<KeyCode> hold = new ArrayList<KeyCode>();
	ArrayList<KeyCode> up = new ArrayList<KeyCode>();
	Scene scene;

	public KeyInput(Scene scene) {
		this.scene = scene;
		EventHandler<KeyEvent> eh = new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent e) {
				handleEvent(e);
			}
		};
		scene.setOnKeyPressed(eh);
		scene.setOnKeyReleased(eh);
	}

	public void listen() {

		up.clear();
		down.clear();

		//System.out.println(hold);
	}

	/**
	 * Tjekker om en tast trykkes, holdes nede eller slippes.
	 */
	public void handleEvent(KeyEvent e) {
		if (e.getEventType() == KeyEvent.KEY_PRESSED) 
		{
			down.add(e.getCode());
			if (!containsKey(hold,e.getCode())) {
				hold.add(e.getCode());
			}
			e.consume();
		}
		else if(e.getEventType() == KeyEvent.KEY_RELEASED){
			hold.remove(e.getCode());
			up.add(e.getCode());
			e.consume();
		}
	}


	private boolean containsKey(ArrayList<KeyCode> a, KeyCode keyCode) {
		for (KeyCode k : a) {
			if (k.equals(keyCode)) {
				return true;
			}
		}
		return false;
	}
	/**
	 * Undersøges om en tast trykkes
	 */
	public boolean getKeyDown(KeyCode keyCode) {
		return containsKey(down,keyCode);
	}
	/**
	 * Undersøges om en tast holdes nede
	 */
	public boolean getKey(KeyCode keyCode) {
		return containsKey(hold,keyCode);
	}
	/**
	 * Undersøges om en tast slippes
	 */
	public boolean getKeyUp(KeyCode keyCode) {
		return containsKey(up,keyCode);
	}

}
